import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import AuthContainer from './containers/AuthContainer';

class App extends Component {
    render() {
        return (
            <div>
                <AuthContainer />
            </div>
        );
    }
}

export default App;
