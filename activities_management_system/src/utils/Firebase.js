import firebase from 'firebase';
import firestore from 'firebase/firestore';
import { firebaseConfig } from './constants/FirebaseConstant';

// initialize the firebase of the provided constant key
firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();
export { db };
export default firebase;
