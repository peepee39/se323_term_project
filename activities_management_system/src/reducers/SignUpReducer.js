import { USER_REQUEST_SIGNUP } from '../actions/actionList';

const INITIALIZED_STATE = {
    isSignUpSuccessful: false,
    userId: '',
};

const userRequestRegister = (state, action) => {
    return {
        ...state,
        isSignUpSuccessful: true,
        userId: action.userId,
    };
};

export default (state = INITIALIZED_STATE, action) => {
    switch (action.type) {
        case USER_REQUEST_SIGNUP:
            return userRequestRegister(state, action);
        default:
            return state;
    }
};
