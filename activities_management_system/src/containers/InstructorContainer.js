import { connect } from 'react-redux';
import Instructor from '../components/main-components/Instructor';
import {
    loadEnrolledStudentList,
    gradeStudent,
    loadGradedStudentList,
} from '../actions/InstructorAction';

const mapStateToProps = (state) => {
    const { user, instructor } = state;
    const { isUserLoggedIn, userRole, userId, realName } = user;
    const { studentEnrolledList, graded_student } = instructor;
    return {
        isUserLoggedIn,
        userRole,
        userId,
        studentEnrolledList,
        realName,
        graded_student,
    };
};

const mapDispatchToProps = (dispatch: Function) => {
    return {
        loadEnrolledStudentList: (instructorName: String) => {
            dispatch(loadEnrolledStudentList(instructorName));
        },
        toGradeStudent: (id: String, student: Object) => {
            dispatch(gradeStudent(id, student));
        },
        loadGradedStudentList: (id: String) => {
            dispatch(loadGradedStudentList(id));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Instructor);
