import { connect } from 'react-redux';
import { loadAllEnrolledActivities } from '../actions/studentAction';
import StudentEnrolledCourse from '../components/main-components/StudentEnrolledCourse';

const mapStateToProps = (state) => {
    const { student, user } = state;
    const { userId } = user;
    const { showToStudentActivityLists, enrolled_activity } = student;
    return {
        showToStudentActivityLists,
        enrolled_activity,
        userId,
    };
};
const mapDispatchToProps = (dispatch: Function) => {
    return {
        loadAllEnrolledActivities: (id: String) => {
            dispatch(loadAllEnrolledActivities(id));
        },
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(StudentEnrolledCourse);
