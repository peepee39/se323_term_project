import { connect } from 'react-redux';
import Header from '../components/sub-components/Header';
import { userRequestLogout } from '../actions/userAction';
import { goTo } from '../actions/studentAction';

const mapStateToProps = (state) => {
    const { user } = state;
    const { isUserLoggedIn, userId, userRole, realName } = user;
    return {
        isUserLoggedIn,
        userId,
        userRole,
        realName,
    };
};
const mapDispatchToProps = (dispatch: Function) => {
    return {
        userRequestLogout: () => {
            dispatch(userRequestLogout());
        },
        goTo: (path: String) => {
            dispatch(goTo(path));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
