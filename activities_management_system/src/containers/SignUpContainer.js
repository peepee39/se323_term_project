import { connect } from "react-redux";
import { SignUp } from "../components/sub-components/SignUp";
import { userRequestLogout, userRequestRegister } from "../actions/UserAction";

const mapStateToProps = state => {
  const { signup } = state;
  const { isSignUpSuccessful } = signup;
  return {
    isSignUpSuccessful
  };
};

const mapDispatchToProps = (dispatch: Function) => {
  return {
    userRequestLogout: () => {
      dispatch(userRequestLogout());
    },
    userRequestRegister: (email: String, password: string) => {
      dispatch(userRequestRegister(email, password));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
