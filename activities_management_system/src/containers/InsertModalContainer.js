import { connect } from 'react-redux';
import InsertModal from '../components/sub-components/InsertModal';

const mapStateToProps = (state: Object) => {
    const { user } = state;
    const { isModalShow } = user;

    return {
        isModalShow,
    };
};
/*const mapDispatchToProps = (dispatch: Function) => {
    return {
        addNewActivity: (id: string, activity: Object) => {
            dispatch(addNewActivity(id, activity));
        },
    };
};
 */
export default connect(mapStateToProps, null)(InsertModal);
