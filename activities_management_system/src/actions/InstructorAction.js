import { db } from '../utils/Firebase';
import _ from 'lodash';
import firebase from '../utils/Firebase';
import { USERS_COLLECTION } from '../constants/firestoreConstants';
import {
    LOAD_ENROLLED_STUDENT_LIST,
    GRADE_STUDENT,
    LOAD_GRADED_STUDENT_LIST,
} from '../constants/constants';

const firestoreRef = db.collection(USERS_COLLECTION);

export const loadEnrolledStudentList = (instructorName: String) => {
    return async (dispatch: Function, getState: Function) => {
        try {
            await firestoreRef
                .where('role', '==', 'student')
                .get()
                .then((querySnapshot) => {
                    let studentEnrolledList = [];
                    let isEnrolled = false;
                    querySnapshot.forEach(function (doc) {
                        console.log(doc.id, ' => ', doc.data());
                        const data = doc.data();
                        const enrolledList = data.enrolled_activity;
                        _.forEach(enrolledList, (value) => {
                            if (_.isEqual(instructorName, value.instructor)) {
                                let studentScore =
                                    _.toInteger(value.num_hour) * 4;
                                const helperList = _.assign({
                                    id: value.id,
                                    instructor: value.instructor,
                                    studentName: data.real_name,
                                    email: data.username,
                                    activity: value.activity_name,
                                    activityId: value.activity_id,
                                    studentScore: studentScore,
                                    studentGrade: _gradeStudentByScore(
                                        studentScore
                                    ),
                                    activityCredit: value.activity_credit,
                                });
                                studentEnrolledList.push(helperList);
                                isEnrolled = true;
                            } else {
                                let isEnrolled = false;
                            }
                        });
                    });
                    if (isEnrolled) {
                        dispatch({
                            type: LOAD_ENROLLED_STUDENT_LIST,
                            studentEnrolledList: studentEnrolledList,
                        });
                    }
                })
                .catch(function (error) {
                    console.log('Error getting documents: ', error);
                });
        } catch (error) {
            console.warn(error);
        }
    };
};

const _gradeStudentByScore = (studentScore: Number) => {
    if (studentScore > 80 && studentScore <= 100) {
        return 'A';
    } else if (studentScore > 70) {
        return 'B';
    } else if (studentScore > 60) {
        return 'C';
    } else if (studentScore > 50) {
        return 'D';
    } else {
        return 'F';
    }
};

export const gradeStudent = (id: string, student: Object) => {
    return async (dispatch: Function, getState: Function) => {
        try {
            _.forEach(student, async (row) => {
                await firestoreRef
                    .doc(id)
                    .update({
                        graded_student: firebase.firestore.FieldValue.arrayUnion(
                            {
                                activity: row.activity,
                                activityId: row.activityId,
                                id: row.id,
                                instructor: row.instructor,
                                studentName: row.studentName,
                                studentScore: row.studentScore,
                                studentGrade: row.studentGrade,
                                activityCredit: row.activityCredit,
                            }
                        ),
                    })
                    .then(() => {
                        dispatch({
                            type: GRADE_STUDENT,
                            userId: id,
                        });
                        alert('Graded Successfully');

                        console.log('Graded Successfully');
                    });
            });
        } catch (error) {
            console.warn(error);
        }
    };
};
export const loadGradedStudentList = (id: string) => {
    return async (dispatch: Function, getState: Function) => {
        try {
            await firestoreRef
                .doc(id)
                .onSnapshot((doc) => {
                    const data = doc.data();
                    const graded_student = data.graded_student;
                    dispatch({
                        type: LOAD_GRADED_STUDENT_LIST,
                        graded_student: graded_student,
                    });
                })
                .then(() => {
                    console.log('Loading graded sucessfully');
                });
        } catch (error) {
            console.warn(error);
        }
    };
};
