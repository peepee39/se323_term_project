// User authentication
export const USER_REQUEST_LOGIN = 'USER_REQUEST_LOGIN';
export const USER_REQUEST_LOGOUT = 'USER_REQUEST_LOGOUT';
export const USER_REQUEST_SIGNUP = 'USER_REQUEST_SIGNUP';

// Product constant for called
export const ADD_PRODUCT = 'ADD_PRODUCT';
export const DELETE_PRODUCT = 'ADD_PRODUCT';
export const UPDATE_PRODUCT = 'ADD_PRODUCT';
export const READ_PRODUCT = 'ADD_PRODUCT';

// Hide modal Show modal for called
export const HIDE_MODAL = 'HIDE_MODAL';
export const SHOW_MODAL = 'SHOW_MODAL';
