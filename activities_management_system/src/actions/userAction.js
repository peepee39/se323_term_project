import { db } from '../utils/Firebase';
import firebase from '../utils/Firebase';
import { USER_REQUEST_LOGIN, USER_REQUEST_LOGOUT } from './actionList';
import _ from 'lodash';
import { USERS_COLLECTION } from '../constants/firestoreConstants';
import {
    ADD_NEW_ACTIVITY,
    LOAD_ALL_ACTIVITIES,
    REMOVE_ACTIVITY,
    UPDATE_ACTIVITY,
    LOAD_INSTRUCTOR_LIST,
} from '../constants/constants';

const firestoreRef = db.collection(USERS_COLLECTION);
export const userRequestLoginWithEmailAndPassword = (
    email: string,
    password: string
) => {
    return async (dispatch: Function, getState: Function) => {
        try {
            await firestoreRef
                .get()
                .then((querySnapshot) => {
                    let realNameList = [];
                    _.remove(realNameList);
                    querySnapshot.forEach((doc) => {
                        const username = doc.data().username;
                        const userId = doc.id;
                        const pass = doc.data().password;
                        const role = doc.data().role;
                        const realName = doc.data().real_name;
                        realNameList.push(realName);
                        const activityLists = doc.data().activity_lists;
                        if (
                            _.isEqual(username, email) &&
                            _.isEqual(pass, password)
                        ) {
                            dispatch({
                                type: USER_REQUEST_LOGIN,
                                userId: userId,
                                username: username,
                                userRole: role,
                                activityLists: activityLists,
                                realName: realName,
                                realNameList: realNameList,
                            });
                        }
                        console.warn('Username or passsword is not correct!');
                    });
                })
                .catch((error) => {
                    console.warn(error);
                });
        } catch (error) {
            console.warn(error);
            alert('Email or password might be incorrect !');
        }
    };
};

export const loadInstructorList = () => {
    return async (dispatch: Function, getState: Function) => {
        try {
            await firestoreRef
                .get()
                .then((querySnapshot) => {
                    let realNameList = [];
                    querySnapshot.forEach((doc) => {
                        const realName = doc.data().real_name;
                        const role = doc.data().role;
                        if (_.isEqual(role, 'instructor')) {
                            realNameList.push(realName);
                        }
                        dispatch({
                            type: LOAD_INSTRUCTOR_LIST,
                            realNameList: realNameList,
                        });
                        console.log('Load instructor list', realNameList);
                    });
                })
                .catch((error) => {
                    console.warn(error);
                });
        } catch (error) {
            console.warn(error);
        }
    };
};

export const userRequestLogout = () => {
    return (dispatch: Function, getState: Function) => {
        dispatch({
            type: USER_REQUEST_LOGOUT,
            userId: '',
            username: '',
            userRole: '',
            activityLists: {},
            realName: '',
            realNameList: [],
        });
    };
};

// Admin actions

export const addNewActivity = (id: string, activity: Object) => {
    return async (dispatch: Function, getState: Function) => {
        try {
            await firestoreRef
                .doc(id)
                .update({
                    activity_lists: firebase.firestore.FieldValue.arrayUnion(
                        activity
                    ),
                })
                .then(() => {
                    dispatch({
                        type: ADD_NEW_ACTIVITY,
                        userId: id,
                    });
                    console.log('Adding sucessfully', activity);
                    this.loadAllActivities(id);
                });
        } catch (error) {
            console.warn(error);
        }
    };
};

export const loadAllActivities = (id: string) => {
    return async (dispatch: Function, getState: Function) => {
        try {
            await firestoreRef
                .doc(id)
                .onSnapshot((doc) => {
                    const data = doc.data();
                    const realName = doc.data().real_name;
                    const activityLists = data.activity_lists;
                    dispatch({
                        type: LOAD_ALL_ACTIVITIES,
                        activityLists: activityLists,
                        realName: realName,
                    });
                })
                .then(() => {
                    console.log('Loading sucessfully');
                });
        } catch (error) {
            console.warn(error);
        }
    };
};

export const removeActivity = (id: string, activity: Object) => {
    return (dispatch: Function, getState: Function) => {
        try {
            _.forEach(activity, async (value) => {
                await firestoreRef
                    .doc(id)
                    .update({
                        activity_lists: firebase.firestore.FieldValue.arrayRemove(
                            {
                                academic_year: value.academic_year,
                                activity_credit: value.activity_credit,
                                activity_id: value.activity_id,
                                activity_name: value.activity_name,
                                activity_status: value.activity_status,
                                id: value.id,
                                instructor: value.instructor,
                                max_enrollment: value.max_enrollment,
                                num_hour: value.num_hour,
                                semester: value.semester,
                            }
                        ),
                    })
                    .then(() => {
                        console.log(`toRemoveActivity ${value.activity_name}`);
                        dispatch({
                            type: REMOVE_ACTIVITY,
                            userId: id,
                        });
                    });
            });
        } catch (error) {
            console.warn(error);
        }
    };
};

export const updateActivity = (id: string, activity: Object) => {
    return async (dispatch: Function, getState: Function) => {
        console.log('activity', activity);
        try {
            const updateActivityId = activity.id;
            await firestoreRef
                .doc(id)
                .onSnapshot((doc) => {
                    const data = doc.data();
                    const activityLists = data.activity_lists;
                    _.forEach(activityLists, async (oldActivity, key) => {
                        const remoteId = oldActivity.id;
                        if (_.isEqual(remoteId, updateActivityId)) {
                            console.log(remoteId, updateActivityId);

                            try {
                                await firestoreRef
                                    .doc(id)
                                    .update({
                                        activity_lists: firebase.firestore.FieldValue.arrayRemove(
                                            {
                                                academic_year:
                                                    oldActivity.academic_year,
                                                activity_credit:
                                                    oldActivity.activity_credit,
                                                activity_id:
                                                    oldActivity.activity_id,
                                                activity_name:
                                                    oldActivity.activity_name,
                                                activity_status:
                                                    oldActivity.activity_status,
                                                id: oldActivity.id,
                                                instructor:
                                                    oldActivity.instructor,
                                                max_enrollment:
                                                    oldActivity.max_enrollment,
                                                num_hour: oldActivity.num_hour,
                                                semester: oldActivity.semester,
                                            }
                                        ),
                                    })
                                    .then(async () => {
                                        dispatch({
                                            type: UPDATE_ACTIVITY,
                                            userId: id,
                                        });
                                        console.log('updated successfully');
                                    });
                            } catch (error) {
                                console.warn(error);
                            }
                        }
                    });
                })
                .then(() => {
                    console.log('Updated sucessfully');
                });
        } catch (error) {
            console.warn(error);
        }
    };
};
