import React, { Component } from 'react';
import _ from 'lodash';
import './css/StudentRegister.css';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import InsertModal from '../sub-components/InsertModal';

class Admin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            enrolledActivity: {},
            isModalVisible: false,
            selectedRow: [],
        };
    }
    componentDidMount() {
        const { loadAllActivities, userId, loadInstructorList } = this.props;
        loadAllActivities(userId);
        loadInstructorList();
    }
    _handleModalVisible = () => {
        this.setState({
            isModalVisible: true,
        });
    };

    _handleModalInvisible = () => {
        this.setState({
            isModalVisible: false,
        });
    };

    _customizeModalHeader(onClose, onSave, openModal) {
        const headerStyle = {
            backgroundColor: '##cca55e',
        };
        return (
            <div className="modal-header" style={headerStyle} onClick={null}>
                <h5>ADD NEW CARD</h5>
            </div>
        );
    }
    _createCustomInsertButton = (openModal) => {
        return (
            <button
                style={{ backgroundColor: '#b87700' }}
                onClick={() => this._handleModalVisible()}
            >
                New
            </button>
        );
    };

    _customizeModalFooter = (onClose, onSave) => {
        const style = {
            backgroundColor: '#ffffff',
        };
        return (
            <div className="modal-footer" style={style}>
                <button className="btn btn-xs btn-secondary" onClick={onClose}>
                    Close
                </button>
                <button className="btn btn-xs btn-info" onClick={onSave}>
                    Save
                </button>
            </div>
        );
    };

    _renderPaginationPanel = (props) => {
        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        {props.components.sizePerPageDropdown}
                    </div>
                    <div className="col-md-auto">
                        {props.components.pageList}
                    </div>
                </div>
            </div>
        );
    };
    _onInsertRow(row) {}

    _onDeleteRow(rowKeys, rows) {
        const { removeActivity, userId } = this.props;
        removeActivity(userId, rows);
    }

    _onSelectRow(row, isSelected) {
        let selectRows = _.cloneDeep(this.state.selectedRow);
        if (!isSelected) {
            selectRows.pop(row);
        } else {
            selectRows.push(row);
        }
        this.setState({
            selectedRow: selectRows,
        });
        console.log(`is selected: ${isSelected}, ${selectRows}`);
    }

    _onSelectAll(isSelected, rows) {
        const selectRows = _.concat(this.state.selectedRow, rows);
        this.setState({
            selectedRow: !isSelected ? [] : selectRows,
        });
        console.log('rows', this.state.selectedRow);
    }

    _onAfterSaveCell(row, cellName, cellValue) {
        const { updatectivity, userId, addNewActivity } = this.props;

        const activity = _.assign({
            id: new Date(),
            activity_id: row.activity_id,
            activity_name: row.activity_name,
            activity_credit: row.activity_credit,
            num_hour: row.num_hour,
            semester: row.semester,
            academic_year: row.academic_year,
            instructor: row.instructor,
            max_enrollment: row.max_enrollment,
            activity_status: row.activity_status,
        });
        addNewActivity(userId, activity);

        updatectivity(userId, row);
    }

    render() {
        const {
            activityLists,
            userId,
            addNewActivity,
            realNameList,
        } = this.props;
        const { isModalVisible } = this.state;
        const selectRowProp = {
            mode: 'checkbox',
            clickToSelect: true,
            bgColor: 'rgb(238, 193, 213)',
            onSelect: this._onSelectRow.bind(this),
            onSelectAll: this._onSelectAll.bind(this),
        };

        const cellEdit = {
            mode: 'dbclick',
            blurToSave: true,
            afterSaveCell: this._onAfterSaveCell.bind(this),
        };
        const options = {
            insertModalHeader: this._customizeModalHeader,
            insertModalFooter: this._customizeModalFooter,
            paginationPanel: this._renderPaginationPanel,
            afterInsertRow: this._onInsertRow.bind(this),
            afterDeleteRow: this._onDeleteRow.bind(this),
            insertBtn: this._createCustomInsertButton,
        };
        const activityData = _.orderBy(activityLists, ['id'], ['desc']);
        const helperList = [];
        _.remove(helperList);

        console.log('realNameList', realNameList);

        return (
            <div className="register-container">
                {!_.isEmpty(activityData) ? (
                    <BootstrapTable
                        data={activityData}
                        selectRow={selectRowProp}
                        cellEdit={cellEdit}
                        options={options}
                        insertRow
                        deleteRow
                        exportCSV
                        search
                        hover
                        pagination
                        headerStyle={{
                            background: '#50b6bb',
                        }}
                    >
                        <TableHeaderColumn
                            dataField="id"
                            isKey
                            hidden
                            dataAlign="center"
                        >
                            id
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            dataField="activity_id"
                            width="50"
                            dataAlign="center"
                        >
                            ID
                        </TableHeaderColumn>

                        <TableHeaderColumn
                            dataField="activity_name"
                            width="420"
                            dataAlign="center"
                        >
                            Name
                        </TableHeaderColumn>

                        <TableHeaderColumn
                            dataField="activity_credit"
                            width="70"
                            dataAlign="center"
                            dataSort
                            editable={{
                                type: 'select',
                                options: {
                                    values: ['1', '2', '3'],
                                },
                            }}
                        >
                            Credit
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            dataField="num_hour"
                            dataSort
                            dataAlign="center"
                            width="80"
                        >
                            Hours
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            dataField="max_enrollment"
                            dataSort
                            dataAlign="center"
                            width="90"
                        >
                            Accept
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            dataField="semester"
                            width="100"
                            dataAlign="center"
                            editable={{
                                type: 'select',
                                options: {
                                    values: ['1', '2', 'Summer'],
                                },
                            }}
                        >
                            Semester
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            dataField="academic_year"
                            dataAlign="center"
                            editable={{
                                type: 'select',
                                options: {
                                    values: ['2020', '2021', '2022', '2023'],
                                },
                            }}
                        >
                            Year
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            dataAlign="center"
                            dataField="instructor"
                            width="175"
                            editable={{
                                type: 'select',
                                options: {
                                    values: realNameList,
                                },
                            }}
                        >
                            Instructor
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            dataField="activity_status"
                            dataAlign="center"
                            editable={{
                                type: 'select',
                                options: {
                                    values: ['Opened', 'Closed', 'Pending'],
                                },
                            }}
                        >
                            Status
                        </TableHeaderColumn>
                    </BootstrapTable>
                ) : null}
                <InsertModal
                    isShowed={isModalVisible}
                    close={this._handleModalInvisible}
                    userId={userId}
                    addNewActivity={addNewActivity}
                    activityLists={activityLists}
                />
            </div>
        );
    }
}

export default Admin;
