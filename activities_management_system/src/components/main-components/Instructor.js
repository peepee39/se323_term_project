import React, { Component } from 'react';
import './css/StudentRegister.css';
import _ from 'lodash';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Tabs, Tab, Table } from 'react-bootstrap';
import Select from 'react-select';

class Instructor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            enrolledActivity: [],
            key: 'studentList',
            selectedRow: [],
            gradedStudent: [],
            selectedValue: 'Jaramed',

           
        };
    }

    componentDidMount() {
        const {
            loadEnrolledStudentList,
            realName,
            loadGradedStudentList,
            userId,
        } = this.props;
        loadEnrolledStudentList(realName);
        loadGradedStudentList(userId);
        console.log('instructor name', realName);
    }
    _setKey = (key) => {
        return this.setState({ key: key });
    };
    _onSelectRow(row, isSelected) {
        let selectRows = _.cloneDeep(this.state.selectedRow);
        if (!isSelected) {
            selectRows.pop(row);
        } else {
            selectRows.push(row);
        }
        this.setState({
            selectedRow: selectRows,
        });
        this._gradeStudentHelper(selectRows);
        console.log(`is selected: ${isSelected}, ${selectRows}`);
    }

    _onSelectAll(isSelected, rows) {
        const { selectedRow } = this.state;
        const selectRows = _.concat(selectedRow, rows);
        this.setState({
            selectedRow: !isSelected ? [] : selectRows,
        });
        this._gradeStudentHelper(selectRows);
        console.log('rows', selectedRow);
    }

    _gradeStudentHelper = (students: Object) => {
        const { gradedStudent } = this.state;
        _.remove(gradedStudent);
        _.forEach(students, (row) => {
            const student = _.assign({
                activity: row.activity,
                activityId: row.activityId,
                email: row.email,
                id: row.id,
                instructor: row.instructor,
                studentName: row.studentName,
                studentScore: row.studentScore,
                studentGrade: row.studentGrade,
                activityCredit: row.activityCredit,
            });

            gradedStudent.push(student);
        });
        console.log(gradedStudent);
    };

    _gradeStudentByScore = (credit: String, grade: String) => {
        switch (grade) {
            case 'A':
                return _.toInteger(credit) * 4;
                break;
            case 'B':
                return _.toInteger(credit) * 3;
                break;
            case 'C':
                return _.toInteger(credit) * 2;
                break;
            case 'D':
                return _.toInteger(credit) * 1;
                break;

            default:
                return _.toInteger(credit) * 0;
                break;
        }
    };

    _createCustomGradeButton = (openModal) => {
        const { gradedStudent } = this.state;

        return (
            <button
                style={{ backgroundColor: '#F4A460', color: 'White' }}
                disabled={_.isEmpty(gradedStudent)}
                onClick={() => this._gradeStudentConfirmation(gradedStudent)}
            >
                Grade student
            </button>
        );
    };

    _gradeStudentConfirmation(students: Object) {
        const { toGradeStudent, userId } = this.props;
        if (window.confirm('Are you sure you wish to these activity?')) {
            toGradeStudent(userId, students);
        }
    }

    handleChange = (selectedOption) => {
        this.setState({ selectedValue: selectedOption.label });
        console.log(`Option selected:`, selectedOption);
    };

    _renderPaginationPanel = (props) => {
        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        {props.components.sizePerPageDropdown}
                    </div>
                    <div className="col-md-auto">
                        {props.components.pageList}
                    </div>
                </div>
            </div>
        );
    };

    render() {
        const { key, selectedValue } = this.state;
        const { studentEnrolledList, graded_student } = this.props;
        const selectRowProp = {
            mode: 'checkbox',
            clickToSelect: true,
            bgColor: 'rgb(238, 193, 213)',
            onSelect: this._onSelectRow.bind(this),
            onSelectAll: this._onSelectAll.bind(this),
        };
        const options = {
            paginationPanel: this._renderPaginationPanel,
            deleteBtn: this._createCustomGradeButton,
        };
        const studentData = _.differenceWith(
            studentEnrolledList,
            graded_student,
            _.isEqual
        );
        const groupedGradeStudent = _.groupBy(graded_student, 'email');
        const nameList = [];
        const sumGradeList = [];
        _.forEach(groupedGradeStudent, (value, key) => {
            nameList.push({ label: key, value: key });
            console.log('sumGradeList', value);
        });
        _.forEach(groupedGradeStudent[selectedValue], (value) => {
            const credit = _.toInteger(value.activityCredit);
            const gradePoint = this._gradeStudentByScore(
                value.activityCredit,
                value.studentGrade
            );
            sumGradeList.push({
                credit: credit,
                gradePoint: gradePoint,
            });
        });

        return (
            <div className="register-container">
                <Tabs
                    id="controlled-tab-example"
                    activeKey={key}
                    onSelect={(k) => this._setKey(k)}
                >
                    <Tab eventKey="studentList" title="Student list">
                        <div className="register-container">
                            {!_.isEmpty(studentData) ? (
                                <div>
                                    <div>
                                        <BootstrapTable
                                            data={studentData}
                                            exportCSV
                                            hover
                                            options={options}
                                            selectRow={selectRowProp}
                                            deleteRow
                                            headerStyle={{
                                                background: '#50b6bb',
                                            }}
                                        >
                                            <TableHeaderColumn
                                                dataField="id"
                                                isKey
                                                hidden
                                                dataAlign="center"
                                            >
                                                id
                                            </TableHeaderColumn>
                                            <TableHeaderColumn
                                                dataField="activityId"
                                                width="50"
                                                dataAlign="center"
                                            >
                                                ID
                                            </TableHeaderColumn>

                                            <TableHeaderColumn
                                                dataField="studentName"
                                                width="350"
                                                dataAlign="center"
                                            >
                                                Name
                                            </TableHeaderColumn>

                                            <TableHeaderColumn
                                                dataField="email"
                                                width="170"
                                                dataAlign="center"
                                                dataSort
                                            >
                                                Email
                                            </TableHeaderColumn>
                                            <TableHeaderColumn
                                                dataField="activity"
                                                dataSort
                                                dataAlign="center"
                                                width="275"
                                            >
                                                Activity
                                            </TableHeaderColumn>

                                            <TableHeaderColumn
                                                dataAlign="center"
                                                dataField="instructor"
                                                width="175"
                                            >
                                                Instructor
                                            </TableHeaderColumn>
                                            <TableHeaderColumn
                                                dataAlign="center"
                                                dataField="studentScore"
                                                width="175"
                                            >
                                                Score
                                            </TableHeaderColumn>
                                            <TableHeaderColumn
                                                dataAlign="center"
                                                dataField="studentGrade"
                                                width="175"
                                                hidden
                                            >
                                                Grade
                                            </TableHeaderColumn>
                                            <TableHeaderColumn
                                                dataAlign="center"
                                                dataField="activityCredit"
                                                width="175"
                                                hidden
                                            >
                                                Credit
                                            </TableHeaderColumn>
                                        </BootstrapTable>
                                    </div>
                                </div>
                            ) : (
                                <div className="register-container">
                                    <h4>No students !</h4>{' '}
                                </div>
                            )}
                        </div>
                    </Tab>
                    <Tab eventKey="graded-student" title="Graded student">
                        <div className="register-container">
                            {!_.isEmpty(groupedGradeStudent) ? (
                                <div>
                                    <div className="col-md-4">
                                        <Select
                                            value={selectedValue.label}
                                            defaultValue={nameList[0].value}
                                            onChange={this.handleChange}
                                            options={nameList}
                                        />
                                    </div>
                                    <div>
                                        <BootstrapTable
                                            data={
                                                groupedGradeStudent[
                                                    selectedValue
                                                ]
                                            }
                                            hover
                                            headerStyle={{
                                                background: '#50b6bb',
                                            }}
                                        >
                                            <TableHeaderColumn
                                                dataField="id"
                                                isKey
                                                hidden
                                                dataAlign="center"
                                            >
                                                id
                                            </TableHeaderColumn>
                                            <TableHeaderColumn
                                                dataField="activityId"
                                                width="50"
                                                dataAlign="center"
                                            >
                                                ID
                                            </TableHeaderColumn>

                                            <TableHeaderColumn
                                                dataField="studentName"
                                                width="350"
                                                dataAlign="center"
                                            >
                                                Name
                                            </TableHeaderColumn>

                                            <TableHeaderColumn
                                                dataField="email"
                                                width="210"
                                                dataAlign="center"
                                                dataSort
                                            >
                                                Email
                                            </TableHeaderColumn>
                                            <TableHeaderColumn
                                                dataField="activity"
                                                dataSort
                                                dataAlign="center"
                                                width="275"
                                            >
                                                Activity
                                            </TableHeaderColumn>

                                            <TableHeaderColumn
                                                dataAlign="center"
                                                dataField="instructor"
                                                width="175"
                                                hidden
                                            >
                                                Instructor
                                            </TableHeaderColumn>
                                            <TableHeaderColumn
                                                dataAlign="center"
                                                dataField="studentScore"
                                                width="175"
                                            >
                                                Score
                                            </TableHeaderColumn>
                                            <TableHeaderColumn
                                                dataAlign="center"
                                                dataField="studentGrade"
                                                width="175"
                                            >
                                                Grade
                                            </TableHeaderColumn>
                                            <TableHeaderColumn
                                                dataAlign="center"
                                                dataField="activityCredit"
                                                width="175"
                                            >
                                                Credit
                                            </TableHeaderColumn>
                                        </BootstrapTable>
                                    </div>
                                    <div>
                                        <Table striped bordered hover>
                                            <thead>
                                                <tr>
                                                    <th>Total Summary</th>
                                                    <th>
                                                        Course:{' '}
                                                        {_.size(
                                                            groupedGradeStudent[
                                                                selectedValue
                                                            ]
                                                        )}
                                                    </th>
                                                    <th>
                                                        Credits:{' '}
                                                        {_.sumBy(
                                                            sumGradeList,
                                                            'credit'
                                                        )}
                                                    </th>
                                                    <th>
                                                        GPA:{' '}
                                                        {(
                                                            _.sumBy(
                                                                sumGradeList,
                                                                'gradePoint'
                                                            ) /
                                                            _.sumBy(
                                                                sumGradeList,
                                                                'credit'
                                                            )
                                                        ).toFixed(1)}
                                                    </th>
                                                </tr>
                                            </thead>
                                        </Table>
                                    </div>
                                </div>
                            ) : (
                                <div className="register-container">
                                    <h4>No student grades!</h4>{' '}
                                </div>
                            )}
                        </div>
                    </Tab>
                </Tabs>
            </div>
        );
    }
}

export default Instructor;
