import React, { Component } from 'react';
import _ from 'lodash';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Table } from 'react-bootstrap';

class ScoreAndGrade extends Component {
    constructor(props) {
        super(props);
        this.state = {
            enrolledActivity: [],
        };
    }

    render() {
        const { enrolled_activity } = this.props;
        const helperList = [];
        _.remove(helperList);
        const helperList2 = [];
        _.remove(helperList2);
        if (!_.isEmpty(enrolled_activity)) {
            _.forEach(enrolled_activity, (value) => {
                let hour = _.toNumber(value.num_hour);
                helperList.push(hour);
                let credit = _.toNumber(value.activity_credit);
                helperList2.push(credit);
            });
        }
        const totalHour = _.sum(helperList);
        const totalCredit = _.sum(helperList2);
        return (
            <div className="register-container">
                {!_.isEmpty(enrolled_activity) ? (
                    <div>
                        <div>
                            <BootstrapTable
                                data={enrolled_activity}
                                exportCSV
                                hover
                                headerStyle={{
                                    background: '#50b6bb',
                                }}
                            >
                                <TableHeaderColumn
                                    dataField="id"
                                    isKey
                                    hidden
                                    dataAlign="center"
                                >
                                    id
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField="activity_id"
                                    width="50"
                                    dataAlign="center"
                                >
                                    ID
                                </TableHeaderColumn>

                                <TableHeaderColumn
                                    dataField="activity_name"
                                    width="420"
                                    dataAlign="center"
                                >
                                    Name
                                </TableHeaderColumn>

                                <TableHeaderColumn
                                    dataField="activity_credit"
                                    width="70"
                                    dataAlign="center"
                                    dataSort
                                >
                                    Credit
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField="num_hour"
                                    dataSort
                                    dataAlign="center"
                                    width="80"
                                >
                                    Hours
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField="academic_year"
                                    dataAlign="center"
                                >
                                    Year
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataAlign="center"
                                    dataField="instructor"
                                    width="175"
                                >
                                    Instructor
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField="activity_status"
                                    dataAlign="center"
                                    hidden
                                >
                                    Status
                                </TableHeaderColumn>
                            </BootstrapTable>
                        </div>
                        <div>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>Total Summary</th>
                                        <th>
                                            Course: {_.size(enrolled_activity)}
                                        </th>
                                        <th>Hour: {totalHour}</th>
                                        <th>Score: {totalHour * 2}</th>
                                        <th>Credit: {totalCredit}</th>
                                    </tr>
                                </thead>
                            </Table>
                        </div>
                    </div>
                ) : (
                    <div className="register-container">
                        <h4>No any enrolled activity yet !</h4>{' '}
                    </div>
                )}
            </div>
        );
    }
}

export default ScoreAndGrade;
