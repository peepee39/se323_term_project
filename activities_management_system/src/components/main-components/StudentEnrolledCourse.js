import React, { Component } from 'react';
import _ from 'lodash';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

class StudentEnrolledCourse extends Component {
    constructor(props) {
        super(props);
        this.state = {
            enrolledActivity: [],
            selectedRow: [],
        };
    }
    componentDidMount() {
        const { loadAllEnrolledActivities, userId } = this.props;
        loadAllEnrolledActivities(userId);
    }
    _onSelectRow(row, isSelected) {
        let selectRows = _.cloneDeep(this.state.selectedRow);
        if (!isSelected) {
            selectRows.pop(row);
        } else {
            selectRows.push(row);
        }
        this.setState({
            selectedRow: selectRows,
        });

        console.log(`is selected: ${isSelected}, ${selectRows}`);
    }

    _onDeleteRow(rowKeys, rows) {
        const { userId, removeEnrolledActivity } = this.props;
        removeEnrolledActivity(userId, rows);
    }

    _onSelectAll(isSelected, rows) {
        const { selectedRow } = this.state;
        const selectRows = _.concat(selectedRow, rows);
        this.setState({
            selectedRow: !isSelected ? [] : selectRows,
        });

        console.log('rows', selectedRow);
    }
    _renderPaginationPanel = (props) => {
        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        {props.components.sizePerPageDropdown}
                    </div>
                    <div className="col-md-auto">
                        {props.components.pageList}
                    </div>
                </div>
            </div>
        );
    };

    render() {
        const { enrolled_activity } = this.props;
        const { enrolledActivity } = this.state;
        const selectRowProp = {
            mode: 'checkbox',
            clickToSelect: true,
            bgColor: 'rgb(238, 193, 213)',
            onSelect: this._onSelectRow.bind(this),
            onSelectAll: this._onSelectAll.bind(this),
        };
        const options = {
            paginationPanel: this._renderPaginationPanel,
            afterInsertRow: null,
            afterDeleteRow: this._onDeleteRow.bind(this),
            deleteBtn: this._createCustomDeleteButton,
        };
        _.remove(enrolledActivity);
        _.forEach(enrolled_activity, (row) => {
            enrolledActivity.push({
                id: row.id,
                activity_id: row.activity_id,
                activity_name: row.activity_name,
                activity_credit: row.activity_credit,
                num_hour: row.num_hour,
                semester: row.semester,
                academic_year: row.academic_year,
                instructor: row.instructor,
                max_enrollment: row.max_enrollment,
                activity_status: row.activity_status,
            });
        });

        return (
            <div className="register-container">
                {!_.isEmpty(enrolledActivity) ? (
                    <BootstrapTable
                        data={enrolledActivity}
                        options={options}
                        selectRow={selectRowProp}
                        deleteRow
                        exportCSV
                        search
                        hover
                        pagination
                        headerStyle={{
                            background: '#50b6bb',
                        }}
                    >
                        <TableHeaderColumn
                            dataField="id"
                            isKey
                            hidden
                            dataAlign="center"
                        >
                            id
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            dataField="activity_id"
                            width="50"
                            dataAlign="center"
                        >
                            ID
                        </TableHeaderColumn>

                        <TableHeaderColumn
                            dataField="activity_name"
                            width="420"
                            dataAlign="center"
                        >
                            Name
                        </TableHeaderColumn>

                        <TableHeaderColumn
                            dataField="activity_credit"
                            width="70"
                            dataAlign="center"
                            dataSort
                        >
                            Credit
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            dataField="num_hour"
                            dataSort
                            dataAlign="center"
                            width="80"
                        >
                            Hours
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            dataField="max_enrollment"
                            dataSort
                            dataAlign="center"
                            width="90"
                        >
                            Accept
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            dataField="semester"
                            width="100"
                            dataAlign="center"
                        >
                            Semester
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            dataField="academic_year"
                            dataAlign="center"
                        >
                            Year
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            dataAlign="center"
                            dataField="instructor"
                            width="175"
                        >
                            Instructor
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            dataField="activity_status"
                            dataAlign="center"
                            hidden
                        >
                            Status
                        </TableHeaderColumn>
                    </BootstrapTable>
                ) : (
                    <div className="register-container">
                        <h4>No any enrolled activity yet !</h4>{' '}
                    </div>
                )}
            </div>
        );
    }
}

export default StudentEnrolledCourse;
