import React, { Component } from 'react';
import _ from 'lodash';
import { ButtonToolbar, Navbar, Button, Nav } from 'react-bootstrap';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
        };
    }

    _handleLogout() {
        const { userRequestLogout } = this.props;
        if (window.confirm('Are you sure you wish to logout from system?')) {
            userRequestLogout();
        }
    }
    _goTo(path: String) {
        const { goTo } = this.props;
        goTo(path);
    }

    render() {
        const { userRole, goTo, realName } = this.props;
        const role = _.capitalize(userRole);
        return (
            <div>
                <Navbar
                    bg="info"
                    variant="info"
                    className="collapseOnSelect nav-bar"
                    activeKey={`/${role}`}
                >
                    <Navbar.Brand
                        className="mr-5"
                        style={{
                            justifyContent: 'center',
                            display: 'flex',
                            width: '100%',
                            color: 'white',
                        }}
                        href={`/${role}`}
                    >
                        {`${role} signed in as ${realName}`}
                    </Navbar.Brand>
                    <ButtonToolbar>
                        <Button
                            variant="warning"
                            onClick={this._handleLogout.bind(this)}
                        >
                            Logout
                        </Button>
                    </ButtonToolbar>
                </Navbar>
            </div>
        );
    }
}
export default Header;
