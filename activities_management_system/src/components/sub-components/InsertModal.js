import React, { Component } from 'react';
import _ from 'lodash';
import { Modal, Button, Form, Col, Spinner } from 'react-bootstrap';

class InsertModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            isSpinnerVisible: false,
            activityId: '',
            activityName: '',
            activityCredit: '',
            activityHour: '',
            activitySemester: '',
            activityYear: '',
            activityInstructor: '',
            activityStatus: 'Closed',
            activityMaxEnrollment: '',
        };
    }

    _handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({
            [name]: value,
        });
    };

    _handleInsertionSubmit = (e) => {
        const {
            activityId,
            activityName,
            activityCredit,
            activityHour,
            activitySemester,
            activityYear,
            activityInstructor,
            activityMaxEnrollment,
            activityStatus,
        } = this.state;
        const { userId, close, addNewActivity } = this.props;

        const activity = _.assign({
            id: new Date(),
            activity_id: activityId,
            activity_name: activityName,
            activity_credit: activityCredit,
            num_hour: activityHour,
            semester: activitySemester,
            academic_year: activityYear,
            instructor: activityInstructor,
            max_enrollment: activityMaxEnrollment,
            activity_status: activityStatus,
        });

        addNewActivity(userId, activity);
        close();
        console.log(activity);
    };

    render() {
        const { isShowed, close, activityLists } = this.props;
        console.log('size', _.size(activityLists));

        return (
            <>
                <Modal
                    show={isShowed}
                    animation={true}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter">
                            Register New Activity
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <Form.Row>
                                <Form.Group as={Col} controlId="activityId">
                                    <Form.Label>ID</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="activityId"
                                        placeholder="Enter ID"
                                        onChange={this._handleChange}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group as={Col} controlId="activityName">
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="activityName"
                                        placeholder="Enter name"
                                        onChange={this._handleChange}
                                        required
                                    />
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} controlId="activityCredit">
                                    <Form.Label>Credit</Form.Label>
                                    <Form.Control
                                        as="select"
                                        name="activityCredit"
                                        onChange={this._handleChange}
                                        required
                                    >
                                        <option>Choose...</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                    </Form.Control>
                                </Form.Group>
                                <Form.Group
                                    as={Col}
                                    controlId="activitySemester"
                                >
                                    <Form.Label>Semester</Form.Label>
                                    <Form.Control
                                        as="select"
                                        name="activitySemester"
                                        onChange={this._handleChange}
                                        required
                                    >
                                        <option>Choose...</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                    </Form.Control>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} controlId="activityHour">
                                    <Form.Label>Hour</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="activityHour"
                                        placeholder="Enter activity hour"
                                        onChange={this._handleChange}
                                        required
                                    />
                                </Form.Group>
                                <Form.Group
                                    as={Col}
                                    controlId="activityMaxEnrollment"
                                >
                                    <Form.Label>Max Enrollment</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="activityMaxEnrollment"
                                        placeholder="Enter max enrollment"
                                        onChange={this._handleChange}
                                        required
                                    />
                                </Form.Group>
                            </Form.Row>

                            <Form.Row>
                                <Form.Group
                                    as={Col}
                                    controlId="activityInstructor"
                                >
                                    <Form.Label>Instructor</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="activityInstructor"
                                        placeholder="Enter instructor name"
                                        onChange={this._handleChange}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group as={Col} controlId="activityStatus">
                                    <Form.Label>Status</Form.Label>
                                    <Form.Control
                                        as="select"
                                        name="activityStatus"
                                        onChange={this._handleChange}
                                        required
                                    >
                                        <option>Choose...</option>
                                        <option>Closed</option>
                                        <option>Opened</option>
                                    </Form.Control>
                                </Form.Group>
                                <Form.Group as={Col} controlId="activityYear">
                                    <Form.Label>Year</Form.Label>
                                    <Form.Control
                                        as="select"
                                        name="activityYear"
                                        onChange={this._handleChange}
                                        required
                                    >
                                        <option>Choose...</option>
                                        <option>2019</option>
                                        <option>2020</option>
                                        <option>2021</option>
                                    </Form.Control>
                                </Form.Group>
                            </Form.Row>
                        </Form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Form.Row>
                            <Button
                                variant="primary"
                                type="submit"
                                onClick={this._handleInsertionSubmit}
                            >
                                Submit
                            </Button>
                            <Button variant="warning" onClick={() => close()}>
                                Close
                            </Button>
                        </Form.Row>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }
}

export default InsertModal;
