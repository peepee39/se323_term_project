import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import _ from 'lodash';
import AuthContainer from '../containers/AuthContainer';
import HeaderContainer from '../containers/HeaderContainer';
import {
    ADMIN_ROLE,
    STUDENT_ROLE,
    TEACHER_ROLE,
} from '../constants/constants';
import AdminContainer from '../containers/AdminContainer';
import StudentContainer from '../containers/StudentContainer';
import InstructorContainer from '../containers/InstructorContainer';
import StudentCourseContainer from '../containers/StudentCourseContainer';

class Root extends Component {
    _renderContainer(userRole) {
        switch (userRole) {
            case ADMIN_ROLE:
                return <AdminContainer />;
            case STUDENT_ROLE:
                return <StudentContainer />;
            case TEACHER_ROLE:
                return <InstructorContainer />;
            default:
                return <AuthContainer />;
        }
    }
    render() {
        const { isUserLoggedIn, userRole, userId } = this.props;
        return (
            <Router>
                {isUserLoggedIn && !!userRole && !!userId ? (
                    <HeaderContainer />
                ) : null}
                <Switch>
                    <Route
                        exact
                        path="/*"
                        render={() => this._renderContainer(userRole)}
                    />
                    <Route
                        path={userRole}
                        render={() => this._renderContainer(userRole)}
                    />
                    <Route path="/home" component={StudentCourseContainer} />
                </Switch>
            </Router>
        );
    }
}

export default Root;
